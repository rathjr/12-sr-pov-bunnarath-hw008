import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Myform from './components/Myform';
import {Container,Row} from 'react-bootstrap'
import Mytable from './components/Mytable';
export default class App extends React.Component {

    constructor(props){
        super(props)
        this.state={
            user:[
                {
                    id:1,
                    username: "narath",
                    email:"povbunnarath10@gmail.com",
                    gender:"male",
                    isActive: null
                },
                {
                    id:2,
                    username: "kimhak",
                    email:"tangho12@gmail.com",
                    gender:"female",
                    isActive: null
                },
                {
                    id:3,
                    username: "moni",
                    email:"monisong40@gmail.com",
                    gender:"male",
                    isActive: null
                },
            ]
        }
    }

    adduser=(newuser)=>{
        console.log(newuser);
        let tmp=[...this.state.user]
        tmp.push(newuser);
        this.setState({
            user: tmp
        })
    }

    toggleActive = i => {
        let temp=[...this.state.user]
        //Remove the if statement if you don't want to unselect an already selected item
        if (i === temp[i].isActive) {
            temp[i].isActive=null
        } else {
            temp[i].isActive= i
        }
        this.setState({
            user:temp
        })
        console.log(this.state.user);
      };

      onDelete=()=>{
          let temp=this.state.user.filter(item=>{
            return item.isActive===null
        })
        this.setState({
            user:temp
        })
      }

    render(){
        return (
            <Container>
                <Row>
                <Myform user={this.state.user} adduser={this.adduser} />
                <Mytable user={this.state.user} adduser={this.adduser} toggleActive={this.toggleActive} onDelete={this.onDelete}/>    
                </Row>         
            </Container>
        )
    }

    
}

