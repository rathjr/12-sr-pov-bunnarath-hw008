import React,{ Component } from 'react'
import {Table,Button,Col} from 'react-bootstrap'

export default class Mytable extends Component {

  render(){
    return (
      <>
      <Col xs="5">
        <h3 style={{marginTop:"65px"}}>Table Account</h3>
        <Table style={{width:"100vh"}} striped bordered hover >
  <thead>
    <tr>
      <th>#</th>
      <th>Username</th>
      <th>Email</th>
      <th>Gender </th>
    </tr>
  </thead>
  <tbody>
      {this.props.user.map((item,i)=>(
    <tr style={
      item.isActive === i
        ? { background: 'gray' }
        : { background: 'white' }
    }
    key={i}
    onClick={()=>this.props.toggleActive(i)}>
      <td>{item.id}</td>
      <td>{item.username}</td>
      <td>{item.email}</td>
      <td>{item.gender}</td>
    </tr>
   ))}
     <Button onClick={this.props.onDelete}  variant="danger" style={{marginTop:"9px"}}>Delete</Button>
  </tbody>
</Table>
</Col>
        </>
    );

  }
   
}
