import React, { Component } from 'react'
import { Form, Button, Image, type, Type, Col } from 'react-bootstrap'
export default class Myform extends Component {

  constructor() {
    super()
    this.state = {
      id: 0,
      username: "",
      email: "",
      gender: "",
      password: "",
      isActive: null,
    }
  }

  add = (e) => {
    let index = this.props.user.length
    let a=this.props.user[index-1].id
      this.state.id = a + 1
   
    e.preventDefault();
    // if (this.state.username === "" && this.setState.email === "" && this.state.gender === "") {
    //   alert("empty")
    //   return
    // }
    this.props.adduser(this.state);
    this.setState({ username: "", email: "", gender: "", password: "" })
  }

  validateBtn = () => {
    if (this.state.email !== "" && this.state.password !== "" && this.state.username !== "" && this.gender !== "") {
      return false
    } else {
      return true
    }
  }


  render() {

    return (
      <>
        <div style={{ marginTop: "15px" }}>
          <h5 style={{ float: "left" }}>KSHRD Student</h5>
          <p style={{ float: "right" }}><span style={{ color: "gray" }}>Sign in as :</span> Annonymous </p>
        </div>
        <Col xs="5">
          <Form onSubmit={this.add} style={{ width: "80%", float: "left", marginTop: "10px", marginLeft: "30px" }}>
            <Image style={{ margin: "10px 130px" }} width="100px" height="100" src="https://image.flaticon.com/icons/png/128/599/599305.png" />
            <h2 style={{ textAlign: "center" }}>Create Account</h2>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Username</Form.Label>
              <Form.Control name="username" value={this.state.username} onChange={(e) => this.setState({ username: e.target.value })} type="text" placeholder="username" />
              <Form.Text className="text-muted">
              <p></p>
              </Form.Text>
            </Form.Group>
            <Form.Label>Gender</Form.Label>
            <br />
            <Form.Check
              custome
              inline
              label="Male"
              type="radio"
              value="male"
              name="gender"
              checked={this.state.gender== "male"}
              onChange={(e) => this.setState({ gender: e.target.value })}
            />
            <Form.Check
              custome
              inline
              label="Female"
              type="radio"
              value="female"
              name="gender"
              checked={this.state.gender === "female"}
              onChange={(e) => this.setState({ gender: e.target.value })}
            />
            <Form.Group controlId="formBasicEmail" style={{ marginTop: "12px" }}>
              <Form.Label>Email</Form.Label>
              <Form.Control name="email" value={this.state.email} onChange={(e) => this.setState({ email: e.target.value })} type="email" placeholder="email" />
              <Form.Text className="text-muted">
              <p></p>
              </Form.Text>
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Password</Form.Label>
              <Form.Control name="password" value={this.state.password} type="password" onChange={(e) => this.setState({ password: e.target.value })} placeholder="password" />
              <Form.Text className="text-muted">
              <p></p>
              </Form.Text>
            </Form.Group>

            <Button disabled={this.validateBtn()} variant="primary" type="submit" value="Submit">
              Save
  </Button>
          </Form>
        </Col>
      </>

    )
  }
}
